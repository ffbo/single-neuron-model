# Single Neuron Model

A project to create a single neuron model, an LIF, and populate a new neuroarch database. This should be used to clarify how to initialise a new neuroarch database and examine how to query the database.

## Installation

Requires NeuroArch installation

## Usage

python single_neuron.py

