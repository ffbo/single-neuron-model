import pdb

import networkx as nx
import h5py

import os
import sys

import numpy as np
from pyorient.ogm import Graph, Config

import neuroarch.nk as nk
from neuroarch.conv.nx import nx_to_orient
import neuroarch.models as models
import neuroarch.nxtools as nxtools

# OrientDB credentials
user = 'root'
password = 'root'

# Leaky Ingegrate and Fire Params

params= {      
    "par_V": -0.05,
    "par_Vr": -0.07,
    "par_Vt": -0.01,
    "par_R": 3e7,
    "par_rest":-0.06,
    "par_C": 1e-9
    }

g = nx.DiGraph() 
g.add_nodes_from([0])

g.node[0] = {
    'model': 'LeakyIAF',
    'name': 'neuron_0',
    'extern': True,      
    'public': True,     
    'spiking': True,    
    'selector': '/a[0]', 

    'V': params['par_V'], 
    'Vr': params['par_Vr'],     
    'Vt': params['par_Vt'],   
    'R': params['par_R'],           
    'C': params['par_C'],         
    ### NeuroArch Variables ###
    'Circuit'   : 'Generic',
    'Class'     : 'LeakyIAF' 
    }

# Prepare the graph for NeuroArch
g = g = nx.MultiDiGraph(g)

graph = Graph(Config.from_url('/single_neuron', user, password,
                                           initial_drop=True))

models.create_efficiently(graph, models.Node.registry)
models.create_efficiently(graph, models.Relationship.registry)

g_lpu_na = nk.nk_lpu_to_na(g,'test')
nx_to_orient(graph.client, g_lpu_na)

# Query NeuroArch

# Retrieve all the LeakyIAF neurons:
graph.LeakyIAFs.query().all()

# Retrieve the loaded lpu:
lpu = graph.LPUs.query(name='test').one()

# Retrieve network object:
lpu.owns().get_as('nx')

"""
Causes Error:

PyOrientCommandException: com.orientechnologies.orient.core.exception.OCommandExecutionExceptionjava.lang.ClassCastException - Error on execution of command: sql.select expand($c) let $a = (select expand(oute()) from [#31:0]),$b = (select expand(ine()) from [#31:0]),$c = intersect($a,$b)com.orientechnologies.orient.core.sql.query.OResultSet cannot be cast to com.orientechnologies.common.util.OSupportsContains

"""

# Retrieve Circuit 'Generic'
graph.Circuits.query().all()

"""
Unexpeceted result:

returns []

I expected this to return the 'Generic' circuit. I must be misuderstanding how we relate nodes to objects.

"""





